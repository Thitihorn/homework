package Day2;

public class Lab9 {

	public static void main(String[] args) {
		
		 for (int count = 0; count <= 20; count++) {
	            if (count == 11) // ถ้า count มีค่าเป็น 11 คำสั่ง continue ภายใน if จะทำงาน
	                continue; // เริ่มต้นรอบใหม่โดยไม่สนใจคำสั่งที่เหลือด้านล่าง
	                System.out.printf("%d ", count); 
	        }
	        System.out.println("\nUsed continue 11");

	}

}
